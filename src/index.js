import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';
import { books } from './books';
import Book from './Book';

const BookList = () => {
    return (
        <>
        <h1>Amazon Best Sellers</h1>
        <section className="booklist">
            {/* <EventExamples /> */}
            {books.map((book, index) => {
                return <Book {...book} key={book.id} number={index} />;
            })}
        </section>
        </>
    );
};

/* const EventExamples = () => {
    const handleFormInput = (e) => {
      console.log('e.target');
      console.log('e.target.name');
      console.log('e.target.product');
    };
    const handleButtonClick = () => {
        alert('handle button click');
      };
    const handleFormSubmission = (e) => {
        e.preventDefault();
        console.log('form submitted');
    };
    return (
      <section>
        <form>
        <h2>Typical Form</h2>
        <input
          type='text'
          name='example'
          onChange={handleFormInput}
          style={{ margin: '1rem 0' }}
        />
        <button type='submit' onClick={handleFormSubmission}>submit form</button>
      </form>
      <button type='button' onClick={handleButtonClick}>click me</button>
    </section>
    );
  }; */


const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<BookList />);